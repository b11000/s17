// Array
// Array - a collection of data
// Ex: let studentNumbers = ['2021-1923','2020-1925', '2020-1991']
// Arrays are declared using square bracket '[]', also know as Array Literal.
// 		Additionally, each data stored in an array is called an element.
// 		let grades = [98.5, 94.3, 89.2, 90.1]
// 		
// It is not recommended to have an array with different data types.
// 
// 
// Each element of an array can also be written in a seperate line.
// let myTasks = [
// 		'drink html',
// 		'eat javascript',
// 		'inhale css',
// 		'bake sass',
// ]
// 
// An element within an array can be accessed by using the array variable, appending it with square brackets, then adding an index on where the element is located.
// console.log(grades[1]);
// console.log(computerBrands[3]);
// console.log(grades[20]);
// 
// Indexing in programming starts with zero.
// 
// Manipulating Arrays
 
/**/

console.log("Array Manipulation")

// Basic Array Structure
// Access Elements in an Array - through index
// 
// Two ways to initializa an Array

let array = [1, 2, 3];
console.log(array);

let arr = new Array(1, 2, 3);
console.log(arr);

// console.log(array[0]);
// console.log(array[1]);
// console.log(array[2]);

// Array Manipulation

let count = ["one", "two", "three", "four"];

// Q: How are we going to add a new element at the end of an array

console.log(count.length);
console.log(count[4]);

	// using assignment operator (=)
	count[4] = "five"
	console.log(count);

	// Reassignment
	count[0] = "element"
	console.log(count)

	// Push method array.push()
		// add element at the end of an array
	// console.log(count.push("element"));
	count.push("element")
	console.log(count)

	/*function pushMethod(){
		return count.push(element)
	}*/
	// console.log(pushMethod());
/*	pushMethod("six")
	pushMethod("seven")
	pushMethod("eight")*/

	console.log(count);

	// Pop method array.pop()
		//removes the last element of an array 
	count.pop();
	console.log(count)

	// Q: Can we use pop method to remove "four" element - No, it will still remove the last element no matter what
	count.pop("four")
	console.log(count)

	function popMethod(){
		count.pop()
	}
	popMethod()

	console.log(count)

	// Q:How do we add element at the beginning of an array
		//unshift() It adds one or more elements to the beginning of an array. 
		//Unshift method array.unshift() 
			//adds element	
		count.unshift("hugot")
		console.log(count)

		function unshiftMethod(element){
			return count.unshift(name);
		}

		unshiftMethod("zero")

		console.log(count)

// Can we also remove element at the beginning of an array? -
	// Shift method	array.shift()
	count.shift("five")
	console.log(count) 

	function shiftMethod(){
		count.shift()
	}
	shiftMethod()

	console.log(count)

	// Sort Method array.sort()
	let nums = [15, 32, 61, 130, 230, 13, 34];
	nums.sort();

	console.log(nums)

	// sort nums in a ascending order
	// nums.sort(
	// 	function(previews value, current value){
	// 	
	// 	}
	// 	)

	/*ascending order*/
	nums.sort(
		function(a, b){
			return a- b
		}
		);

	console.log(nums)

	nums.sort(
		function(a, b){
			return b- a
		}
		);

	console.log(nums)

	// Reverse method array.reverse()
	nums.reverse()
	console.log(nums)

	/*Splice and Slice */

	// Splice method 	array.splice()
		// returns an array of omitted elements
		// It directly manipulates the original array

		// first parameter - index where to start omitting element
		// second parameter - # of elements to be omitted starting from first parameter
		// third parameter - elements to be added in place of the omitted elements.
	console.log(count)

	/*let newSplice = count.splice(1);
	console.log(newSplice)//newSplice holds the splice method
	console.log(count)*/

	/*let newSplice = count.splice(1, 2);
	console.log(newSplice);
	console.log(count)*/

	/*let newSplice = count.splice(1, 2, "a1, b2, c3")
	console.log(newSplice);
	console.log(count);*/

	// Slice method array.slice(start, end)

		// first parameter - index where to begin omitting elements
		// second parametr - # of elements to be omitted (index - 1 array.length )

	// let newSlice = count.slice(1);
	// console.log(newSlice);
	// console.log(count);

	let newSlice = count.slice(2, 3);
	console.log(newSlice);
	console.log(count);

		//Concat method   array.concat()
		//use to merge two or more arrays
	console.log(count)
	console.log(nums)
	let animals = ["bird", "cat", "dog", "fish"];

	let newConcat = count.concat(nums, animals)
	console.log(newConcat);


	// Join method array.join() // "", "-", ""
	let meal = ["rice", "steak", "juice"];

	let newJoin = meal.join()
	console.log(newJoin)

	newJoin = meal.join("")
	console.log(newJoin)

	newJoin = meal.join(" ")
	console.log(newJoin)

	newJoin = meal.join("-")
	console.log(newJoin)

	// toString method
	console.log(nums)
	console.log(typeof nums[3])
	// typeof - targetting what type of data

	let newString = nums.toString()
	console.log(newString)//converts it to string

/*Accessors*/
let countries = ["US", "PH", "CAN", "PH", "SG", "HK", "PH", "NZ"];
	//indexof()		array.indexof()
		//finds the index of a give element where it is "first" found.
	let index = countries.indexOf("PH");
	console.log(index);

	// If element is non existing, return is -1
	let lastIndex = countries.lastIndexOf("AU");
	console.log(lastIndex);

	//lastIndexOf()
	lastIndex = countries.lastIndexOf("PH");
	console.log(lastIndex);

	// if(countries.indexOf("CAN") == -1){
	// 	console.log(`Element not existing`)
	// } else {
	// 	console.log(`Element exists in the countries array`)
	// }

/*Iterators*/
	
	// forEach()	array.forEach()
	// map()		array.map()
	// // forEach() method executes a provided function once for each array element.

let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];

	// forEach
		// returns undefined
	days.forEach(
		function(element){
			console.log(element)
		}
	)

	/*let daysOftheweek = days.forEach(
		function(element){
			return element
		}
	)

	console.log(daysOftheweek)*/
	// maps
		// arrays
	let mapDays = days.map(function(day){
		return day + `${day} is the days of the week`
	})

	console.log(mapDays)
	console.log(days)

/* Mini- Activity

	using forEach method, add each element of days array in an empty array called days2

*/

	days2 = days.forEach(
		function (days2){
			console.log(days2);
			}
		);

	// filter		array.filter(cb())
	console.log(nums)
	let nums = [15, 32, 61, 130, 230, 13, 34];
	let newFilter = nums.filter(function(num){
		return num < 50 
	})

	console.log(newFilter)

	// includes 	array.includes()
		// returns boolean value if a word is existing
	console.log(animals)

	let newIncludes = animals.includes("dog");
	console.log(newIncludes)

/* Mini Activity

	create a function that will accept a value, that if value is found in the array, return a message <value> is found
	if value is not found, return `<value> not found!`
	
	Try to invoke function 3x with different values
 */	



function miniActivity(name) {
	if (animals.includes(name) == true ){
		return `${name} is found`
	} else {
		return `${name} is not found`
	}
}

	console.log(miniActivity("cat"));
	console.log(miniActivity("ex"));
	console.log(miniActivity("fish"));

	// every(cb())
		//returns boolean value
		// It returns true only if "all elements" passed the give condition
	let newEvery = nums.every(function(num){
		return ( num > 10 )
	})
	console.log(newEvery);

	// some(cb())
		// boolean
	let newSome = nums.some(function(num){
		return (num > 50)
	})
	console.log(newSome);

	// Example Arrow Function
	// let newSome2 = nums.some(num => num > 50)
	// console.log(newSome2);

	// reduce(cb(<previous>, <current>))
	// a and b is parameter
	let newReduce = nums.reduce(function(a, b){
		return b + a
		// return b- a
	})
	console.log(newReduce)

	// to get the average of nums array
		// total all the elements
		// divide it by total # of elements

	let average = newReduce/ nums.length



	// toFixed(# of decimals) - returns string

	console.log(parseInt(average.toFixed(2)))


	// parseInt() or parseFloat()

	// console.log(parseInt(average.toFixed(2)))
	// console.log(parseFloat(average.toFixed(2)));



// Review
// Arrays are collection of data
// 
// .length(a method)
// 		checks the quantity of data in an array
// 
// .push
// 		add element or data
// 
// .pop
// 		remove element or data
// 		
// splice		
// 		removes or replaces existing elements
// 
// sort
// 		sorts the lements of an array
// 
// reverse
// 		It reverses an array
// 
// slice
// 		It extracts a section of a given array
// 
// map
// 		It takes a function as an argument that will run on all elements on the array
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 












