

// 3. Create an addStudent function that will accept a name of the student and add it to the student array.

let students = ["John", "Jane", "Joe"]

	let addStudent = students.forEach(
		function(addStudent){
			console.log(`addStudent ('${addStudent}')`);
			console.log(`${addStudent} was added to the student's list.`);
		}
	)

// 4. Create a countStudent() function that will print the total number of students in the array.

	let countStudent = students;
	countStudent.forEach(n => console.log(`There are a total of ${(students.length)} students enrolled.`));

// 5. Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.

let printStudents = students;
	printStudents.sort();
/*	console.log(printStudents)*/

	
	printStudents = students
		console.log(`printStudents()`)
	printStudents.forEach(
		function(element){
		console.log(element)
		}
	)

 /*
 	6.Create a findStudent() function that will do the following:
		a. Search for a student name when a keyword is given (filtermethod).
		b. one match is found print the message studentName is an enrollee.
		c. if multiple matches are found print the message studentNames are enrollees.
		d. if no match is found print the message studentName is not an enrollee.
		e. The keyword given should not be case sensitive.
*/


	let filtered = []

	function findStudent(search){		
		search = search.toLowerCase();
		// var findMultipleStudent = 'j',
		filtered = students.filter(function(keyWordFilter){
			// return search.includes(find);
			return search === keyWordFilter.toLowerCase();
		})
		if(filtered == null || filtered ==0) {
			console.log(`findStudent('${search}')`);
			console.log(`No student found with the name ${search}`)
		} else {
		if(filtered.length === 1){
			console.log(`findStudent('${search}')`);
			console.log(`${search} is an Enrollee`)
		} else {
			console.log(`findStudent('${search}')`);
			console.log(`${students} are enrollees`)
		}
		}
	}

findStudent("joe")
findStudent("bill")
findStudent("j")
		

// var myArray = ["bedroomone", 
//               "bedroomonetwo", 
//               "bathroom"];

// var findMultipleStudent = 'j',
//     filtered = students.filter(function (keyWordFilter) {
//       return search.includes(find); 
      
//     });
    
//     console.log(filtered)
 

/*Sample*/
// var myArray = ["bedroomone", 
//               "bedroomonetwo", 
//               "bathroom"];
              

// var PATTERN = 'bedroom',
//     filtered = myArray.filter(function (str) {
//       return str.includes(PATTERN); 
      
//     });     



// Activity: Stretch Goals
/*
	1. Create an addSection() function that will add a section to all students in the array with the format of studentName - Section A(map method).
	2. Createa a removeSTudent() function that will do the following:
		a. Capitalize the first letter of the user's input (toUpperCase and slice methods.)
		b. Retrieve the index of the student to be removed ( indexOf method)
		c. Remove the student from the array (splice method.)
		d. Print a message that the studentName was removed from the student's list. 
 */